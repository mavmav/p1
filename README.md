Hello.

This is the presentation for WT2, Praktikum #1



Should you want to build this yourself, go ahead:

```bash
# run the following:

# install deps
git clone https://gitlab.com/mavmav/p1.git

# apt -y install git-lfs
# pip install hovercraft


cd p1
git lfs install
git lfs fetch

# run dev version (when editing; opens localhost webserver)
hovercraft p1.rst


# create final editable/ presentable output version
#hovercraft p1.rst <OUTDIR>
hovercraft p1.rst p1
```



files put in `images` will be automatically included into the output
directory


optional `css` and `js` files can also be included, but not necessary for
now. 


Once you created an output version, everything is included in the output
dir. 

-------------------------

Copy the output dir or the already processed Pillars.zip to where you want and open the contained `index.html` with your
favorite browser --> You're ready to present!
