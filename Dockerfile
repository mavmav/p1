FROM debian

RUN apt update && apt -y install python3 python3-pip 
RUN apt -y install make pandoc


RUN pip3 install hovercraft

RUN mkdir -p /p1

WORKDIR /p1

COPY . ./


CMD ["make"]
