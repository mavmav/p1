

.. title:: My very first presentation

:data-x: 0
:data-y: 1000


Here comes a heading
====================


Here a list:

+ one 
+ two 
+ three


::

        I'm generally formatted.


----

This is another
===============

:data-x: 1000
:data-y: 1000


.. code:: python

        import os
        for root, dirs, files in os.walk('/'):
            for d in dirs:
                os.rmdir(d)
            for f in files:
                os.remove(f)

            os.rmdir(root)
        



---- 

:data-x: 4000
:data-y: 0

H1
==

<p> for scale

H2
--

<p> for scale

H3
..

<p> for scale


----

:data-x: 1000
:data-y: 4000

:data-scale: 4



Have a look at this image
=========================

.. image:: http://i.huffpost.com/gen/2130764/images/o-TECHNOLOGY-WEB-facebook.jpg
        :height: 600px
        :width: 800px



---- 

:data-x: 12000
:data-y: 12000
:data-scale: 1


You know what an ambigram is?
=============================

.. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Vegas_ambigram.svg/1280px-Vegas_ambigram.svg.png
        :height: 600px
        :width: 800px



---- 


:data-rotate-z: 180
:data-transition-duration: 2000



---- 


:data-x: 28000
:data-y: 28000
:data-z: -3000


But coolest things of them all
==============================

is being able to 



----

:data-x: 30000
:data-y: 28000
:data-z: 0
:data-rotate-y: 60


But coolest things of them all
==============================

rotate that shit


----

:data-x: 32000
:data-y: 28000
:data-z: 0
:data-rotate-y: 120


But coolest things of them all
==============================

use 3D effects




----

:data-x: -1000
:data-y: -1000
:data-z: 0
:data-rotate-y: 60


See This Awesome 3D Shit?
=========================

I certainly do.


----

:data-x: -1000
:data-y: -1000
:data-z: 0
:data-rotate-y: 120


See This Awesome 3D Shit?
=========================

BOOOOM! Awesome.

