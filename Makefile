OUTDIR=p1

.PHONY: serve

serve: 
	hovercraft -c p1.css p1.rst


build:
	hovercraft -c p1.css p1.rst ${OUTDIR}

clean:
	rm -rf ${OUTDIR}
	rm -rf p1.pdf


pdf:	build
	pandoc ${OUTDIR}/index.html -o p1.pdf
